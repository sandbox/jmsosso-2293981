<?php

/**
 * Implements hook_services_resources().
 */
function web_services_helper_services_resources() {
  $resources = array();
  $services  = module_invoke_all('services_classes');

  foreach($services as $service) {
    foreach ($service::getResourceClasses() as $resource) {
      if (method_exists($resource, 'getCrudOperations')) {
        foreach ($resource::getCrudOperations() as $operation) {
          $resources[$resource::getResourceName()]['operations'][$operation->getName()] = $operation->getResource();
        }
      }
      if (method_exists($resource, 'getActions')) {
        foreach ($resource::getActions() as $action) {
          $resources[$resource::getResourceName()]['actions'][$action->getName()] = $action->getResource();
        }
      }
    }
  }

  return $resources;
}


/**
 * Implements hook_ctools_plugin_api().
 */
function web_services_helper_ctools_plugin_api($owner, $api) {
  if ($owner == 'services' && $api == 'services') {
    return array('version' => 3,);
  }
}


/**
 * Implements hook_default_services_endpoint().
 * Add the endpoint to server rest
 */
function web_services_helper_default_services_endpoint() {
  $endpoints = array();
  $services  = module_invoke_all('services_classes');

  foreach($services as $service) {
    $endpoints[] = web_services_helper_get_endpoint($service);
  }

  return $endpoints;
}


function web_services_helper_get_endpoint_resources($class) {
  foreach ($class::getResourceClasses() as $resource) {
    $def = array();

    if (method_exists($resource, 'getCrudOperations')) {
      foreach ($resource::getCrudOperations() as $operation) {
        $def['operations'][$operation->getName()] = array('enabled' => 1);
      }
    }
    if (method_exists($resource, 'getActions')) {
      foreach ($resource::getActions() as $action) {
        $def['actions'][$action->getName()] = array('enabled' => 1);
      }
    }
  }
  $resources[$resource::getResourceName()] = $def;

  return $resources;
}


function web_services_helper_get_endpoint($class) {
  $endpoint = new stdClass();

  $endpoint->disabled        = FALSE;
  $endpoint->api_version     = 3;
  $endpoint->name            = $class::getServiceName();
  $endpoint->server          = 'rest_server';
  $endpoint->path            = $class::getEndpointPath();
  $endpoint->server_settings = array();
  $endpoint->resources       = web_services_helper_get_endpoint_resources($class);
  $endpoint->debug           = 0;

  $endpoint->authentication = array(
    'services' => 'services',
  );

  return $endpoint;
}


function web_services_helper_resource_callback(array $callbackMethod) {
  $params = func_get_args();
  array_shift($params);

  $obj = new $callbackMethod[0];

  return $obj->execute($callbackMethod[1], $params);
}


// For debug
function _watchdog($var) {
  watchdog('debug', '<pre>%msg</pre>', array(
    '%msg' => print_r($var, TRUE)));
}

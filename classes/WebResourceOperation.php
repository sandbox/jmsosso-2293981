<?php

/**
 * Helper to create resource arrays for the Services module
 */
class WebResourceOperation {
  private $name;
  private $method;
  private $help;
  private $args = array();
  private $accessCallback  = 'user_access';
  private $accessArguments = array('access content');


  /**
   * Create a new resource operation.
   * @param string $name Name of the operation. For CRUD operations must be one of create, retrieve, update, delete or index.
   * If you don't use setCallback() to specify the name of the method to call this name is used.
   * @param string $help Help text.
   */
  public function __construct($name, $help = '') {
    $db = debug_backtrace();

    $this->name   = $name;
    $this->method = array($db[1]['class'], $name);  // By default the callback is same as the name of the resource
    $this->help   = $help;
  }

  public function getName() {
    return $this->name;
  }


  /**
   * Sets the method to call when this resource operation is triggered.
   * @param string $methodName Name of the method of the current class to use as a callback.
   */
  public function setCallback($methodName) {
    $db = debug_backtrace();
    $this->method = array($db[1]['class'], $methodName);
  }


  /**
   * Adds a new URL parameter to the resource.
   * @param string $name Parameter name.
   * @param int $index URL part after the resource name starting by 0
   * @param string $type Type of the parameter: boolean, double, int, string, array, struct, date or base64.
   * @param boolean $optional If the parameter is optional. Defaults to FALSE.
   * @param mixed $default If the parameter is optional this is it default value.
   * @link https://drupal.org/node/438416
   */
  public function setPathArgument($name, $index, $type = 'string', $optional = FALSE, $default = NULL) {

    $this->args[] = array(
      'name' => $name,
      'optional' => $optional,
      'source' => array('path' => $index),
      'type' => $type,
      'default value' => $default,
    );
  }

  /**
   * Adds a new data (POST) parameter to the resource.
   * @param string $name Parameter name.
   * @param string $type Type of the parameter: boolean, double, int, string, array, struct, date or base64.
   * @param boolean $optional If the parameter is optional. Defaults to FALSE.
   * @param mixed $default If the parameter is optional this is it default value.
   * @link https://drupal.org/node/438416
   */
  public function setDataArgument($name, $type = 'string', $optional = FALSE, $default = NULL) {

    $this->args[] = array(
      'name' => $name,
      'optional' => $optional,
      'source' => array('data' => $name),
      'type' => $type,
      'default value' => $default,
    );
  }

  /**
   * Adds a new parameter (query string) to the resource.
   * @param string $name Parameter name.
   * @param string $type Type of the parameter: boolean, double, int, string, array, struct, date or base64.
   * @param boolean $optional If the parameter is optional. Defaults to FALSE.
   * @param mixed $default If the parameter is optional this is it default value.
   * @link https://drupal.org/node/438416
   */
  public function setParamArgument($name, $type = 'string', $optional = FALSE, $default = NULL) {

    $this->args[] = array(
      'name' => $name,
      'optional' => $optional,
      'source' => array('param' => $name),
      'type' => $type,
      'default value' => $default,
    );
  }


  public function setAccess($callback = 'user_access', $arguments = NULL) {
    $arguments = func_get_args();
    array_shift($arguments);
    $this->accessCallback  = $callback;
    $this->accessArguments = $arguments;
  }


  /**
   * Creates the resource array for the Services module.
   * @return array Resource specification.
   */
  public function getResource() {

    // First parameter must be the "real" callback as a default value, due that this parameter
    // never will be received by the resource
    array_unshift($this->args, array(
      'name' => '_method',
      'optional' => TRUE,
      'source' => array('data' => '_method'),
      'type' => 'array',
      'default value' => $this->method,
    ));

    return array(
      'help' => $this->help,
      'callback' => 'web_services_helper_resource_callback',
      'access callback' => $this->accessCallback,
      'access arguments' => $this->accessArguments,
      'access arguments append' => FALSE,
      'args' => $this->args,
    );
  }
}
<?php

/**
 * Base class for all resources.
 */
abstract class WebResource {
  const STATUS_OK             = 200;
  const STATUS_BAD_REQUEST    = 400;
  const STATUS_NOT_FOUND      = 404;
  const STATUS_NOT_ACCEPTABLE = 406;
  const STATUS_INTERNAL_ERROR = 500;
  const STATUS_OK_MSG         = 'OK';
  const RELATED_LINKS         = 'relLinks';

  private $links   = array();
  private $lastMsg = '';

  abstract static public function getResourceName();

  /**
   * Sets the returning http status.
   * @param int $code Http status code.
   * @param string $message Status message.
   * @link https://blog.apigee.com/detail/restful_api_design_what_about_errors/
   */
  protected function setStatus($code = self::STATUS_OK, $message = self::STATUS_OK_MSG) {
    drupal_add_http_header('Status', "$code $message");
  }


  /**
   * Returns an array to be used as related links.
   * @param string $relation Type of relation with the actual content: self, next, prev, etc.
   * @param string $path Relative path to the resource.
   * @param array $params Optional query parameters to add to the URI
   * @return array
   */
  protected function createLink($relation, $path, array $params = array()) {
    return array($relation => $this->serviceUrl() . "/$path" . ($params? '?' .
      drupal_http_build_query($params): ''));
  }


  /**
   * Add related links to the resource.
   * @param string $rel Type of relationship with the actual resource.
   * @param string $path Relative path to the resource.
   * @param array $params Optional query parameters to add to the URI
   */
  protected function addLink($rel, $path, array $params = array()) {
    $this->links += $this->createLink($rel, $path, $params);
  }

  /**
   * Standard 404 Not Found
   */
  protected function notFound($message = '') {
    $this->setStatus(self::STATUS_NOT_FOUND, 'Not Found' . ($message? ": $message": ''));
    $this->lastMsg = $message;
  }

  /**
   * Standard 500 Internal Server Error
   */
  protected function internalError() {
    $this->setStatus(self::STATUS_INTERNAL_ERROR, 'Internal Server Error');
  }


  /**
   * Gets web service URL from current server address.
   * @return string
   */
  protected function serviceUrl() {
    return $GLOBALS['base_url'] . '/' . services_server_info_object()->endpoint_path;
  }


  /**
   * Entry point for the service callback. Called from resource_definition_callback() when a
   * request to the resource is received.
   * @param string $method Name of the method in the inherited class to call.
   * @param array $params Parámeters received from the http request to send to the method.
   * @return array Array returned by the method call merged with the status array.
   * The status array is created from the http status code defined by setStatus().
   * @see resource_definition_callback()
   * @see WebResource::setStatus()
   */
  final public function execute($method, array $params) {
    $this->lastMsg = '';
    $data = call_user_func_array(array($this, $method), $params);

    if (is_array($data)) {
      if ($this->links) {
        $data[self::RELATED_LINKS] = $this->links;
      }

      return $data;
    }
    else {
      return array($this->lastMsg);
    }
  }
}

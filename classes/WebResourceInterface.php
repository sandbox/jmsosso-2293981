<?php

/**
 * Interface for all service resources.
 */
interface WebResourceInterface {
  /**
   * Returns the resource name.
   */
  static public function getResourceName();
}

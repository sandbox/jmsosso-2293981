<?php

/**
 * Base class to inherit from to implement your resources
 */
abstract class WebResourceBase extends WebResource implements WebResourceInterface {
}

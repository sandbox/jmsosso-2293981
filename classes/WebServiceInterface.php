<?php

/**
 * Interface for all services
 */
interface WebServiceInterface {
  /**
   * You must return the service name in this method.
   */
  public static function getServiceName();
  /**
   * You must return the endpoint path in this method.
   */
  public static function getEndpointPath();
  /**
   * You must return an array containing all the classes names in which your resources are implemented.
   */
  public static function getResourceClasses();
}
